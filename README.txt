
Summary
-------
The purpose of this module is to provide anonymous users a way to login (or just
supply contact information) during the process of entering the comment. It works
by opening a modal window with the authentication options once they Submit the
comment. Once they are successfully identified, the comment will be submitted
and attributed to the user.

One good reason to use this module is when you want your users to log in for
posting comments, but you don't want to scare away anonymous users. It helps
engage the users by having them type the comment, then dealing on the
authentication right before posting.

Requirements
------------
This module depends on Chaos tool suite (ctools) and Drupal core Comments.

Credits
-------
* Developed by Victor Kareh (vkareh - http://drupal.org/user/144520)
* Sponsored by NBC Universal
